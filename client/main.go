package main

import (
	"net/http"
	"strconv"
)

func main() {
	var (
		ress [100]*http.Response
		err  error
	)
	for i, port := range []string{"8080", "8081"} {
		var numGoroutine int
		for i := 0; i < len(ress); i++ {
			ress[i], err = http.Get("http://localhost:" + port + "/" + strconv.Itoa(i))
			if err != nil {
				println("err:", err.Error())
				return
			}
			defer ress[i].Body.Close()
		}
		for _, res := range ress {
			body := []byte{3: 0}
			n, _ := res.Body.Read(body)
			nr, _ := strconv.Atoi(string(body[:n]))
			if nr > numGoroutine {
				numGoroutine = nr
			}
		}
		println([]string{"nbio_nbhttp", "net_http"}[i], ",num goroutine:", numGoroutine)
	}
}
