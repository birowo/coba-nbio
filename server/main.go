package main

import (
	"net/http"
	"runtime"
	"strconv"

	"github.com/lesismal/nbio/nbhttp"
)

func main() {
	srvr1, srvr2 := "nbio/nbhttp\n", "\n\nnet/http\n"
	hndlr := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cntr := r.URL.Path[1:]
		if cntr == "0" {
			print(srvr1)
			srvr1, srvr2 = srvr2, srvr1
		}
		print(cntr, " ")
		w.Write([]byte(strconv.Itoa(runtime.NumGoroutine())))
	})

	nbhttp.NewServer(nbhttp.Config{
		Network: "tcp",
		Addrs:   []string{":8080"},
		Handler: hndlr,
	}).Start()

	http.ListenAndServe(":8081", hndlr)
}
